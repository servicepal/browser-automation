#!/usr/bin/env python

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import unittest
import sys
import json

class PtwebTestCase(unittest.TestCase):
  def setUp(self):
    self.driver = webdriver.Remote(
      command_executor='http://192.168.2.26:4444/wd/hub',
      desired_capabilities=DesiredCapabilities.FIREFOX)
      #desired_capabilities=DesiredCapabilities.CHROME)

  def test_example(self):
    with open('sites.json') as data:
      sites = json.load(data)
    for site in sites["sites"]:
      print 'getting site: ' + site
      self.driver.get(site)
      f = site.replace('http://', '')
      f = f.replace('www.', '')
      f = f.replace('/', '-')
      print 'saving screenshot to: ./out/selenium/' + f + '.png'
      self.driver.save_screenshot('./out/selenium/' + f + '.png')

  def tearDown(self):
    self.driver.quit()

# python test.py firefox MAC
# python test.py firefox LINUX
if __name__ == "__main__":
  unittest.main()

