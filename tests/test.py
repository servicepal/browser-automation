#!/usr/bin/env python

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import unittest
import sys

class PtwebTestCase(unittest.TestCase):
  #capabilities = None

  def setUp(self):
    self.driver = webdriver.Remote(
      command_executor='http://192.168.2.250:4444/grid/console',
      #desired_capabilities=self.capabilities)
      desired_capabilities=DesiredCapabilities.CHROME)

  def test_example(self):
    self.driver.get("http://www.google.com")
    self.assertEqual(self.driver.title, "Google")

  def tearDown(self):
    self.driver.quit()

# python test.py firefox MAC
# python test.py firefox LINUX
if __name__ == "__main__":
  # "version": "41",
  # "javascriptEnabled": True,
  # "video": True,
  #PtwebTestCase.capabilities = {
    #"browserName": sys.argv[1],
    #"platform": sys.argv[2],
  #}
  unittest.main()

