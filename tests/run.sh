#!/bin/bash -ex

function __selenium() {
  time ./screenium.py
}

function __phantomjs() {
  time ../node_modules/.bin/phantomjs ./screentom.js
}

function __main() {
  __selenium
  __phantomjs
}

__main
