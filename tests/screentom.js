var fs = require('fs');
var data = fs.open('./sites.json', 'r');
var sites = JSON.parse(data.read()).sites;

var counter = 0;

for (site in sites) {
  kacha(sites[site]);
}

function kacha(url) {
  console.log('start fetching site: ' + url);

  var f = url.replace("http://", "");
  f = f.replace("www.", "");
  f = f.replace("/", "-");

  var page = require('webpage').create();
  //viewportSize being the actual size of the headless browser
  page.viewportSize = { width: 1024, height: 768 };
  //the clipRect is the portion of the page you are taking a screenshot of
  page.clipRect = { top: 0, left: 0, width: 1024, height: 768 };
  //the rest of the code is the same as the previous example
  page.open(url, function() {
    console.log('saving screenshot file: ' + f);
    page.render('./out/phantomjs/' + f + '.png');
    ++counter;
    end();
  });
}

function end() {
  if (counter === sites.length)
    phantom.exit();
}

